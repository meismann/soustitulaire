# Soustitulaire

This tool can transform the subtitles in an SRT file by

1. shifting them by a certain offset,
2. stretching them by a certain factor.

# Installation

- Clone this repo and enter its directory.
- Run `bundle install`.
- Run `ruby run.rb -h` to know your options.
