require 'optparse'
require 'active_support/core_ext/string'

options = { diff_points: [] }
option_parser = OptionParser.new do |opts|
  opts.banner = <<~DESC
    Usage: ruby #{__FILE__} [options] SUBTITLEFILE

    This tool can transform the subtitles in an SRT file by

    1. shifting them by a certain offset,
    2. stretching them by a certain factor.

    However, neither offset nor factor are for you to specify; they are calculated when you specify either

    1. one 'diff-point' if you think that shifting by a certain offset is enough, or
    2. two 'diff-points' if it turns out that stretching is necessary, too. In this case, give the -d option twice.

    Options:
  DESC

  opts.on(
    '-d STRING', 
    '--diffpoint=STRING', 
    String, 
    <<~DESC.squish
      A string in the format NN=NN:NN[:NN[.NNN]], e.g. 101=01:12:59.123, 
      where 101 is the index of the subtitle and the time value the desired 
      start time of the subtitle. This option must be specified at least once
      and at most twice.
    DESC
    ) do |d|
    options[:diff_points] << DiffPoint.new(d)
  end
end

class Util
  class << self
    def convert_to_seconds(str)
      time = str.to_time
      time.to_f - time.beginning_of_day.to_f
    rescue
      fail "Error understanding '#{str}' as a time specification"
    end
  end
end

class DiffPoint
  attr_reader :index, :start_time

  def initialize(str)
    index, start_time = str.split('=')

    raise "'#{index}' in '#{str}' must be a plain integer." unless index =~ /^\d+$/
    @index = index.to_i

    @start_time = Util.convert_to_seconds(start_time)
  end
end

class Subtitle < Struct.new(:timeframe, :text)
  attr_reader :start_time, :end_time

  def initialize(timeframe, _)
    super
    @start_time, @end_time = timeframe.split(' --> ').map(&Util.method(:convert_to_seconds))
  end

  def transform!
    @start_time = yield @start_time
    @end_time = yield @end_time
  end

  def to_s
    "#{srt_time_format(@start_time)} --> #{srt_time_format(@end_time)}\n#{text}"
  end

  private

  def srt_time_format(seconds)
    Time.at(seconds).utc.strftime('%H:%M:%S,%L')
  end
end

class SubtitleFile
  def initialize(file_name, *diff_points)
    @file_name = file_name
    @subtitles = subtitles_from(File.read(file_name))
    register_transform_fnc_params(diff_points)
  end

  def transform!
    @subtitles.map do |subtitle|
      subtitle.transform!(&transform_fnc)
    end

    write_to_new_file
  end
    
  def to_s
    @subtitles.map.with_index do |subtitle, i|
      "#{i + 1}\n#{subtitle}"
    end .join("\n\n")
  end

  private

  def subtitles_from(srt)
    srt.strip.split(/(?:\A.?|[\n\r]+)\d+[\n\r]+/).reject(&:empty?).map do |subtitle|
      Subtitle.new *subtitle.strip.split(/[\r\n]+/, 2)
    rescue
      fail "Subtitle \n\n#{subtitle}\n\n could not be parsed. Abandon."
    end
  end

  # Remapping the subtitles uses a linear function in the form
  #
  # y = mx + n
  #
  # to map old, i.e. given time values to new, desired ones, by stretching
  # and/or shifting them along the time scale. The function thus reads:
  #
  # new_time_value = stretch_factor * old_time_value + offset.
  #
  # Calculating slope (m or stretch_factor) and y-intercept (n or offset) follows
  # the usual pattern of constructing a linear function from two given points 
  # in a cartesian plane.
  def register_transform_fnc_params(diff_points)
    old_start_seconds_1 = @subtitles[diff_points[0].index - 1].start_time
    new_start_seconds_1 = diff_points[0].start_time

    if diff_points[1]
      old_start_seconds_2 = @subtitles[diff_points[1].index - 1].start_time
      new_start_seconds_2 = diff_points[1].start_time

      @fnc_slope = (new_start_seconds_2 - new_start_seconds_1) / (old_start_seconds_2 - old_start_seconds_1)
    else
      @fnc_slope = 1
    end

    @fnc_intercept = new_start_seconds_1 - @fnc_slope*old_start_seconds_1
  end

  def transform_fnc
    @transform_fnc ||= lambda do |old_start_seconds|
      old_start_seconds * @fnc_slope + @fnc_intercept
    end
  end

  def write_to_new_file
    new_file_name = @file_name.sub(/^(.+)(\.[^.]+)$/, "\\1 (offset=#{@fnc_intercept.round(1)}, stretch=#{@fnc_slope.round(4)})\\2")
    File.write(new_file_name, to_s)
  end
end

option_parser.parse!

unless (file_name = ARGV.pop)
  fail 'No file given. Check with -h option for proper format.'
end

unless (1..2).include?(options[:diff_points].size)
  fail 'The -d options must be specified at least once and at most twice.'
end

file = SubtitleFile.new file_name, *options[:diff_points]
file.transform!
